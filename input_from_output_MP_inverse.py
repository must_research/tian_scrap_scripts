import torch
import numpy as np
import torch.nn as nn
import torchvision.datasets as dsets
import torchvision.transforms as transforms
from torch.autograd import Variable
from matplotlib import pyplot as plt

# This is a silly script to try and obtain the "ideal" input for a given output in the MNIST problem.
# It attempts to train a small (one hidden layer of 100 units) network as per usual and then extract the transform
# matrix. Thereafter, an approximated inverse transform matrix is generated using the Moore-Penrose Inverse.
# This is then multiplied to a given "desired" output array to obtain the "perfect input".
# However, it is clear that a "perfect" input for the network is not always coherent to the human eye.
# This is probably due to the fact that a wide (more columns than rows) transform matrix results in a MP-inverse that
# provides one of many solutions, which has no guaranty of being the coherent and visually pleasing image we desire.
# More involved methods will be required to solve this problem.

# Train a network (not my original code)________________________________________________________________________________
# Hyper Parameters
input_size = 784
hidden_size = 100
num_classes = 10
num_epochs = 5
batch_size = 100
learning_rate = 0.0001

# MNIST Dataset
train_dataset = dsets.MNIST(root='./data',
                            train=True,
                            transform=transforms.ToTensor(),
                            download=True)

test_dataset = dsets.MNIST(root='./data',
                           train=False,
                           transform=transforms.ToTensor())

# Data Loader (Input Pipeline)
train_loader = torch.utils.data.DataLoader(dataset=train_dataset,
                                           batch_size=batch_size,
                                           shuffle=True)

test_loader = torch.utils.data.DataLoader(dataset=test_dataset,
                                          batch_size=batch_size,
                                          shuffle=False)


# Neural Network Model (1 hidden layer)
class Net(nn.Module):
    def __init__(self, input_size, hidden_size, num_classes):
        super(Net, self).__init__()
        self.fc1 = nn.Linear(input_size, hidden_size)
        #self.relu = nn.ReLU()
        self.fc2 = nn.Linear(hidden_size, num_classes)

    def forward(self, x):
        out = self.fc1(x)
        #out = self.relu(out)
        out = self.fc2(out)
        return out


net = Net(input_size, hidden_size, num_classes)

# Loss and Optimizer
criterion = nn.CrossEntropyLoss()
optimizer = torch.optim.Adam(net.parameters(), lr=learning_rate)

# Train the Model
for epoch in range(num_epochs):
    for i, (images, labels) in enumerate(train_loader):
        # Convert torch tensor to Variable
        images = Variable(images.view(-1, 28 * 28))
        labels = Variable(labels)

        # Forward + Backward + Optimize
        optimizer.zero_grad()  # zero the gradient buffer
        outputs = net(images)
        loss = criterion(outputs, labels)
        loss.backward()
        optimizer.step()

        if (i + 1) % 100 == 0:
            print ('Epoch [%d/%d], Step [%d/%d], Loss: %.4f'
                   % (epoch + 1, num_epochs, i + 1, len(train_dataset) // batch_size, loss.data[0]))

# Test the Model
correct = 0
total = 0
for images, labels in test_loader:
    images = Variable(images.view(-1, 28 * 28))
    outputs = net(images)
    _, predicted = torch.max(outputs.data, 1)
    total += labels.size(0)
    correct += (predicted == labels).sum()

print('Accuracy of the network on the 10000 test images: %d %%' % (100 * correct / total))

# Obtain the "transform" matrix from trained model______________________________________________________________________

the_trans_matrix = np.dot(np.array(net.fc2.weight.data), np.array(net.fc1.weight.data))

# test the matrix with simple examples__________________________________________________________________________________
# one_image = np.zeros((28, 28))
# c = 0
# for y in range(4, 24):
#     for x in range(12, 16):
#         one_image[y][x] = 1
#         c += 1
# one_data = np.zeros((784))
# c = 0
# for y in range(0, 28):
#     for x in range(0, 28):
#         one_data[c] = one_image[y][x]
#         c += 1
# one_out = np.dot(the_trans_matrix, one_data)
# plt.imshow(one_image, interpolation='nearest')
# plt.title("the model thinks this is a " + str(one_out.argmax()))
# plt.show()

# Get the inverse of the transform matrix_______________________________________________________________________________
M = the_trans_matrix
M_i = np.linalg.pinv(M)

# Get the "ideal" inputs for each class_________________________________________________________________________________
for n in range(0, 10):
    O = ([[0], [0], [0], [0], [0], [0], [0], [0], [0], [0]])
    O[n] = [1]
    I = np.dot(M_i, O)
    image = np.ones((28, 28))
    i = 0
    for y in xrange(0, 28):
        for x in xrange(0, 28):
            image[y][x] = I[i][0]
            i += 1
    new_o = np.dot(M, I)
    plt.imshow(image, interpolation='nearest')
    plt.title("This is the best input for a " + str(new_o.argmax()))
    plt.text(x, y, new_o)
    plt.show()
