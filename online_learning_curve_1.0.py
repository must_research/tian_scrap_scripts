import matplotlib
matplotlib.use('TkAgg')
#matplotlib.use('Agg')
import matplotlib.pyplot as plt
import numpy as np



class NosyGraphLogger:

    def __init__(self):
        self.step_count = 0

        self.plot_data_list = []
        self.plot_data_list.append((DataToPlot(i_title="Training", i_x_a="Epoch", i_y_a="Accuracy", i_colour='-g'),
                                    DataToPlot(i_title="Validation", i_x_a="Epoch", i_y_a="Accuracy", i_colour='-b')))
        self.plot_data_list.append((DataToPlot(i_title="Train Loss", i_x_a="Epoch", i_y_a="Loss", i_colour='-r'),))
        self.plot_data_list.append((DataToPlot(i_title="Generalisation Gap", i_x_a="Epoch", i_y_a="*Accuracy", i_colour='-m'),))
        self.plot_data_list.append((DataToPlot(i_title="Sparsity", i_x_a="Epoch", i_y_a="Sparsity", i_colour='-y'),))
        self.plot_data_list.append((DataToPlot(i_title="Weights", i_x_a="Epoch", i_y_a="Weight Values", i_colour='-g',
                                               i_d_type='histogram2D'),))
        self.plot_data_list.append((DataToPlot(i_title="Gradients", i_x_a="Epoch", i_y_a="Gradient Values", i_colour='-g',
                                               i_d_type='histogram2D'),))

        self.sparsity_threshold = 0.01
        self.the_fig = plt.figure()

    def feed_grapher(self,
                     train_accuracy_val=None,
                     validation_accuracy_val=None,
                     train_loss_val=None,
                     model_state=None,
                     sparsity=False,
                     generalisation_gap=False,
                     weight_distribution=False,
                     gradient_distribution=False,
                     online_plot=True):

        if train_accuracy_val is not None:
            self.plot_data_list[0][0].data_list.append(train_accuracy_val)
            self.plot_data_list[0][0].to_plot = True
        if validation_accuracy_val is not None:
            self.plot_data_list[0][1].data_list.append(validation_accuracy_val)
            self.plot_data_list[0][1].to_plot = True
        if train_loss_val is not None:
            self.plot_data_list[1][0].data_list.append(train_loss_val)
            self.plot_data_list[1][0].to_plot = True
        if train_accuracy_val is not None and validation_accuracy_val is not None and generalisation_gap:
            self.plot_data_list[2][0].data_list.append(train_accuracy_val-validation_accuracy_val)
            self.plot_data_list[2][0].to_plot = True
        if model_state is not None and sparsity:
            self.plot_data_list[3][0].data_list.append(self.get_sparsity(model_state))
            self.plot_data_list[3][0].to_plot = True
        if model_state is not None and weight_distribution:
            self.plot_data_list[4][0].data_list.append(self.get_weight_distribution(model_state))
            self.plot_data_list[4][0].to_plot = True
        if model_state is not None and gradient_distribution:
            self.plot_data_list[5][0].data_list.append(self.get_gradient_distribution(model_state))
            self.plot_data_list[5][0].to_plot = True
        self.step_count += 1
        if online_plot is True:
            self.quick_plot_nested_graph()

    def quick_plot_nested_graph(self):
        self.the_fig.clear()
        plot_count = self.determine_plot_count()
        grid_shape = self.determine_grid_shape(plot_count)
        x_ax = []
        for val in range(0, self.step_count):
            x_ax.append(val+1)
        n = 1
        for data_to_plot in self.plot_data_list:
            is_to_be_plotted = False
            for p in data_to_plot:
                if p.to_plot:
                    is_to_be_plotted = True
            if is_to_be_plotted:
                plot = self.the_fig.add_subplot(grid_shape[0], grid_shape[1], n)
                plot.grid(True, which='both', axis='both')
                dp_legend = []
                plot_type = ''
                for p in data_to_plot:
                    done = False
                    if p.to_plot:
                        plot_type = p.data_type
                        dp_legend.append(p.title)
                        if plot_type is 'function':
                            plot.plot(x_ax, p.data_list, p.colour)
                            done = True
                        elif plot_type is 'histogram':
                            plot.hist(x=p.data_list[self.step_count-1], bins=1000)
                            done = True
                        elif plot_type is 'histogram2D':
                            plt.grid(False)
                            hist_through_time, hist_edges = self.get_2D_hist(p.data_list)
                            plot.imshow(hist_through_time, interpolation='nearest', aspect='auto',
                                        extent=[1, self.step_count, np.max(hist_edges), np.min(hist_edges)],
                                        cmap='CMRmap')
                        plot.set_xlabel(p.x_axis_name)
                        plot.set_ylabel(p.y_axis_name)
                        done = True
                if done:
                    n += 1
                if plot_type is 'function':
                    plot.legend(dp_legend)
        plt.pause(0.001)
        plt.show(block=False)

    def plot_nested_graph(self):
        plt.close("all")
        self.the_fig.clear()
        fig = plt.figure(figsize=(10, 9))
        fig.canvas.set_window_title('learning_info')
        plot_count = self.determine_plot_count()
        grid_shape = self.determine_grid_shape(plot_count)
        x_ax = []
        for val in range(0, self.step_count):
            x_ax.append(val + 1)
        n = 1
        for data_to_plot in self.plot_data_list:
            is_to_be_plotted = False
            for p in data_to_plot:
                if p.to_plot:
                    is_to_be_plotted = True
            if is_to_be_plotted:
                plot = fig.add_subplot(grid_shape[0], grid_shape[1], n)
                plot.grid(True, which='both', axis='both')
                dp_legend = []
                plot_type = ''
                done = False
                for p in data_to_plot:
                    done = False
                    if p.to_plot:
                        plot_type = p.data_type
                        dp_legend.append(p.title)
                        if plot_type is 'function':
                            plot.plot(x_ax, p.data_list, p.colour)
                            done = True
                        elif plot_type is 'histogram':
                            plot.hist(x=p.data_list[self.step_count - 1], bins=1000)
                            done = True
                        elif plot_type is 'histogram2D':
                            plt.grid(False)
                            hist_through_time, hist_edges = self.get_2D_hist(p.data_list)
                            plot.imshow(hist_through_time, interpolation='nearest', aspect='auto',
                                        extent=[1, self.step_count, np.max(hist_edges), np.min(hist_edges)],
                                        cmap='CMRmap')
                        plot.set_xlabel(p.x_axis_name)
                        plot.set_ylabel(p.y_axis_name)
                        done = True
                if done:
                    n += 1
                if plot_type is 'function':
                    plot.legend(dp_legend)
        plt.pause(0.001)
        plt.show()

    def get_sparsity(self, model=None):
        if model is None:
            return None
        else:
            threshold = self.sparsity_threshold
            zeros = 0
            total = 0
            for param in model.parameters():
                if len(param.data.shape) > 1:
                    for row in param.data:
                        for val in row:
                            if np.abs(float(val)) <= threshold:
                                zeros += 1
                            total += 1
            spar = float(zeros)/float(total)
            return spar

    def get_weight_distribution(self, model=None):
        if model is None:
            return None
        else:
            all_weights = []
            for param in model.parameters():
                if len(param.data.shape) > 1:
                    for row in param.data:
                        for val in row:
                            all_weights.append(val)
            return all_weights

    def get_gradient_distribution(self, model=None):
        if model is None:
            return None
        else:
            all_gradients = []
            for param in model.parameters():
                if len(param.data.shape) > 1:
                    for row in param.grad.data:
                        for val in row:
                            all_gradients.append(val)
            return all_gradients

    def determine_plot_count(self):
        plot_count = 0

        for dtp in self.plot_data_list:
            flag = False
            for p in dtp:
                if p.to_plot:
                    flag = True
                    break
            if flag:
                plot_count += 1

        return plot_count

    def determine_grid_shape(self, n):
        col_n = np.floor(np.sqrt(n))
        row_n = np.ceil(n / col_n)
        shape = (row_n, col_n)
        return shape

    def get_2D_hist(self, data_lists):

        w = len(data_lists)
        max_val = np.max(data_lists)
        min_val = np.min(data_lists)
        h = 200
        ret_hist = []
        edges = []
        first = True
        for epoch_data in data_lists:
            the_hist = np.histogram(epoch_data, bins=h, range=(min_val, max_val))
            if first:
                n = 0
                for val in the_hist[0]:
                    ret_hist.append([])
                    ret_hist[n].append(val)
                    n += 1
                edges = the_hist[1]
                first = False
            else:
                n = 0
                for val in the_hist[0]:
                    ret_hist[n].append(val)
                    n += 1

        # #trim
        # trim_threshold = 0.01
        # trim_count = 1
        # stencil = []
        # for row in ret_hist:
        #     dead_row = True
        #     for val in row:
        #         if val > trim_count:
        #             dead_row = False
        #             break
        #     stencil.append(dead_row)
        # for n in range(0, len(stencil)):
        #     if stencil[n]:
        #         ret_hist = ret_hist[1:]
        #         edges = edges[1:]
        #     else:
        #         break
        # for n in range(len(stencil)-1, 0, -1):
        #     if stencil[n]:
        #         ret_hist = ret_hist[0:(len(ret_hist)-2)]
        #         edges = edges[0:(len(edges) - 2)]
        #     else:
        #         break






        return ret_hist, edges


class DataToPlot:

    def __init__(self, i_title="Accuracy", i_x_a="Epoch", i_y_a="Accuracy", i_colour='-b', i_d_type='function'):
        self.data_list = []
        self.x_axis_name = i_x_a
        self.y_axis_name = i_y_a
        self.title = i_title
        self.colour = i_colour
        self.to_plot = False
        self.data_type = i_d_type

