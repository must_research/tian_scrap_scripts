import torch
import numpy as np
import torch.nn as nn
from torch.autograd import Variable

# This script is intended to verify that the proposed method of implementing L1 and L2 regularization corresponds with
# our theoretical understanding (see p.224 to p.228 of Goodfellow) of the concepts.
# It simulates one forward and one backward pass in a very simple MLP with a controlled input and label.
# This is done through analytical means and using the autograd functionality of Pytorch, independently.
# The idea is that if the updated weights correspond (analytical vs. autograd) with varying
# L1 and L2 penalty coefficients, as well as learning rates, then the implementation is sound. Manipulating the three
# values, listed above, will show that this is indeed the case.

# The specific network is constructed as follows:
# input(2x1) > M1(5x2) > hidden_layer(5x1) > M2(2x5) > output(2x1)

# Hyper Parameters______________________________________________________________________________________________________
input_vector = np.array([[0.6000], [0.4000]], dtype="<f")
label_vector = np.array([[1.0000], [0.0000]], dtype="<f")
M1 = np.array([[-0.0981, -0.3572], [-0.1943, 0.3367], [-0.0721, 0.6006], [-0.4013, -0.0207], [-0.0800, 0.5175]], dtype="<f")
M2 = np.array([[-0.1718, 0.3940, 0.3893, 0.3688, -0.0757], [0.2330, 0.1268, 0.4181, -0.4401, -0.4243]], dtype="<f")
input_size = 2
width_size = 5
num_classes = 2
learning_rate = 0.0015
l1_coef = 0.5000
l2_coef = 0.5000
# Analytical truths_____________________________________________________________________________________________________
# Forward
H_g = np.dot(M1, input_vector)
O_g = np.dot(M2, H_g)
loss_g = 0.5 * (np.power((O_g[0]-label_vector[0]), 2)+np.power((O_g[1]-label_vector[1]), 2)) + l1_coef * (np.sum(np.absolute(M1))+np.sum(np.absolute(M2))) + l2_coef * (0.5*np.sum(np.square(M1))+0.5*np.sum(np.square(M2)))
# Backward
# M2 gradients
M2_11_grad = (O_g[0]-1)*(H_g[0])
M2_12_grad = (O_g[0]-1)*(H_g[1])
M2_13_grad = (O_g[0]-1)*(H_g[2])
M2_14_grad = (O_g[0]-1)*(H_g[3])
M2_15_grad = (O_g[0]-1)*(H_g[4])
M2_21_grad = (O_g[1])*(H_g[0])
M2_22_grad = (O_g[1])*(H_g[1])
M2_23_grad = (O_g[1])*(H_g[2])
M2_24_grad = (O_g[1])*(H_g[3])
M2_25_grad = (O_g[1])*(H_g[4])
# M2 updates
new_M2_11_g = (1-learning_rate*l2_coef)*M2[0][0] - learning_rate*l1_coef*np.sign(M2[0][0]) - learning_rate * M2_11_grad
new_M2_12_g = (1-learning_rate*l2_coef)*M2[0][1] - learning_rate*l1_coef*np.sign(M2[0][1]) - learning_rate * M2_12_grad
new_M2_13_g = (1-learning_rate*l2_coef)*M2[0][2] - learning_rate*l1_coef*np.sign(M2[0][2]) - learning_rate * M2_13_grad
new_M2_14_g = (1-learning_rate*l2_coef)*M2[0][3] - learning_rate*l1_coef*np.sign(M2[0][3]) - learning_rate * M2_14_grad
new_M2_15_g = (1-learning_rate*l2_coef)*M2[0][4] - learning_rate*l1_coef*np.sign(M2[0][4]) - learning_rate * M2_15_grad
new_M2_21_g = (1-learning_rate*l2_coef)*M2[1][0] - learning_rate*l1_coef*np.sign(M2[1][0]) - learning_rate * M2_21_grad
new_M2_22_g = (1-learning_rate*l2_coef)*M2[1][1] - learning_rate*l1_coef*np.sign(M2[1][1]) - learning_rate * M2_22_grad
new_M2_23_g = (1-learning_rate*l2_coef)*M2[1][2] - learning_rate*l1_coef*np.sign(M2[1][2]) - learning_rate * M2_23_grad
new_M2_24_g = (1-learning_rate*l2_coef)*M2[1][3] - learning_rate*l1_coef*np.sign(M2[1][3]) - learning_rate * M2_24_grad
new_M2_25_g = (1-learning_rate*l2_coef)*M2[1][4] - learning_rate*l1_coef*np.sign(M2[1][4]) - learning_rate * M2_25_grad
new_M2 = np.array([[new_M2_11_g[0], new_M2_12_g[0], new_M2_13_g[0], new_M2_14_g[0], new_M2_15_g[0]], [new_M2_21_g[0], new_M2_22_g[0], new_M2_23_g[0], new_M2_24_g[0], new_M2_25_g[0]]], dtype="<f")

# M1 gradients
M1_11_grad = (M2_11_grad*((O_g[0]-M2[0][1]*H_g[1]-M2[0][2]*H_g[2]-M2[0][3]*H_g[3]-M2[0][4]*H_g[4])/(H_g[0]*H_g[0]))*input_vector[0])+(M2_21_grad*((O_g[1]-M2[1][1]*H_g[1]-M2[1][2]*H_g[2]-M2[1][3]*H_g[3]-M2[1][4]*H_g[4])/(H_g[0]*H_g[0]))*input_vector[0])
M1_12_grad = (M2_11_grad*((O_g[0]-M2[0][1]*H_g[1]-M2[0][2]*H_g[2]-M2[0][3]*H_g[3]-M2[0][4]*H_g[4])/(H_g[0]*H_g[0]))*input_vector[1])+(M2_21_grad*((O_g[1]-M2[1][1]*H_g[1]-M2[1][2]*H_g[2]-M2[1][3]*H_g[3]-M2[1][4]*H_g[4])/(H_g[0]*H_g[0]))*input_vector[1])
M1_21_grad = (M2_12_grad*((O_g[0]-M2[0][0]*H_g[0]-M2[0][2]*H_g[2]-M2[0][3]*H_g[3]-M2[0][4]*H_g[4])/(H_g[1]*H_g[1]))*input_vector[0])+(M2_22_grad*((O_g[1]-M2[1][0]*H_g[0]-M2[1][2]*H_g[2]-M2[1][3]*H_g[3]-M2[1][4]*H_g[4])/(H_g[1]*H_g[1]))*input_vector[0])
M1_22_grad = (M2_12_grad*((O_g[0]-M2[0][0]*H_g[0]-M2[0][2]*H_g[2]-M2[0][3]*H_g[3]-M2[0][4]*H_g[4])/(H_g[1]*H_g[1]))*input_vector[1])+(M2_22_grad*((O_g[1]-M2[1][0]*H_g[0]-M2[1][2]*H_g[2]-M2[1][3]*H_g[3]-M2[1][4]*H_g[4])/(H_g[1]*H_g[1]))*input_vector[1])
M1_31_grad = (M2_13_grad*((O_g[0]-M2[0][0]*H_g[0]-M2[0][1]*H_g[1]-M2[0][3]*H_g[3]-M2[0][4]*H_g[4])/(H_g[2]*H_g[2]))*input_vector[0])+(M2_23_grad*((O_g[1]-M2[1][0]*H_g[0]-M2[1][1]*H_g[1]-M2[1][3]*H_g[3]-M2[1][4]*H_g[4])/(H_g[2]*H_g[2]))*input_vector[0])
M1_32_grad = (M2_13_grad*((O_g[0]-M2[0][0]*H_g[0]-M2[0][1]*H_g[1]-M2[0][3]*H_g[3]-M2[0][4]*H_g[4])/(H_g[2]*H_g[2]))*input_vector[1])+(M2_23_grad*((O_g[1]-M2[1][0]*H_g[0]-M2[1][1]*H_g[1]-M2[1][3]*H_g[3]-M2[1][4]*H_g[4])/(H_g[2]*H_g[2]))*input_vector[1])
M1_41_grad = (M2_14_grad*((O_g[0]-M2[0][0]*H_g[0]-M2[0][1]*H_g[1]-M2[0][2]*H_g[2]-M2[0][4]*H_g[4])/(H_g[3]*H_g[3]))*input_vector[0])+(M2_24_grad*((O_g[1]-M2[1][0]*H_g[0]-M2[1][1]*H_g[1]-M2[1][2]*H_g[2]-M2[1][4]*H_g[4])/(H_g[3]*H_g[3]))*input_vector[0])
M1_42_grad = (M2_14_grad*((O_g[0]-M2[0][0]*H_g[0]-M2[0][1]*H_g[1]-M2[0][2]*H_g[2]-M2[0][4]*H_g[4])/(H_g[3]*H_g[3]))*input_vector[1])+(M2_24_grad*((O_g[1]-M2[1][0]*H_g[0]-M2[1][1]*H_g[1]-M2[1][2]*H_g[2]-M2[1][4]*H_g[4])/(H_g[3]*H_g[3]))*input_vector[1])
M1_51_grad = (M2_15_grad*((O_g[0]-M2[0][0]*H_g[0]-M2[0][1]*H_g[1]-M2[0][2]*H_g[2]-M2[0][3]*H_g[3])/(H_g[4]*H_g[4]))*input_vector[0])+(M2_25_grad*((O_g[1]-M2[1][0]*H_g[0]-M2[1][1]*H_g[1]-M2[1][2]*H_g[2]-M2[1][3]*H_g[3])/(H_g[4]*H_g[4]))*input_vector[0])
M1_52_grad = (M2_15_grad*((O_g[0]-M2[0][0]*H_g[0]-M2[0][1]*H_g[1]-M2[0][2]*H_g[2]-M2[0][3]*H_g[3])/(H_g[4]*H_g[4]))*input_vector[1])+(M2_25_grad*((O_g[1]-M2[1][0]*H_g[0]-M2[1][1]*H_g[1]-M2[1][2]*H_g[2]-M2[1][3]*H_g[3])/(H_g[4]*H_g[4]))*input_vector[1])
# M1 update
new_M1_11_g = (1-learning_rate*l2_coef)*M1[0][0] - learning_rate*l1_coef*np.sign(M1[0][0]) - learning_rate * M1_11_grad
new_M1_12_g = (1-learning_rate*l2_coef)*M1[0][1] - learning_rate*l1_coef*np.sign(M1[0][1]) - learning_rate * M1_12_grad
new_M1_21_g = (1-learning_rate*l2_coef)*M1[1][0] - learning_rate*l1_coef*np.sign(M1[1][0]) - learning_rate * M1_21_grad
new_M1_22_g = (1-learning_rate*l2_coef)*M1[1][1] - learning_rate*l1_coef*np.sign(M1[1][1]) - learning_rate * M1_22_grad
new_M1_31_g = (1-learning_rate*l2_coef)*M1[2][0] - learning_rate*l1_coef*np.sign(M1[2][0]) - learning_rate * M1_31_grad
new_M1_32_g = (1-learning_rate*l2_coef)*M1[2][1] - learning_rate*l1_coef*np.sign(M1[2][1]) - learning_rate * M1_32_grad
new_M1_41_g = (1-learning_rate*l2_coef)*M1[3][0] - learning_rate*l1_coef*np.sign(M1[3][0]) - learning_rate * M1_41_grad
new_M1_42_g = (1-learning_rate*l2_coef)*M1[3][1] - learning_rate*l1_coef*np.sign(M1[3][1]) - learning_rate * M1_42_grad
new_M1_51_g = (1-learning_rate*l2_coef)*M1[4][0] - learning_rate*l1_coef*np.sign(M1[4][0]) - learning_rate * M1_51_grad
new_M1_52_g = (1-learning_rate*l2_coef)*M1[4][1] - learning_rate*l1_coef*np.sign(M1[4][1]) - learning_rate * M1_52_grad
new_M1 = np.array([[new_M1_11_g[0], new_M1_12_g[0]], [new_M1_21_g[0], new_M1_22_g[0]], [new_M1_31_g[0], new_M1_32_g[0]], [new_M1_41_g[0], new_M1_42_g[0]], [new_M1_51_g[0], new_M1_52_g[0]]], dtype="<f")

# With Pytorch__________________________________________________________________________________________________________
# Define network
class Net(nn.Module):
    def __init__(self, input_size, width_size, num_classes):
        super(Net, self).__init__()
        self.fc1 = nn.Linear(input_size, width_size, bias=False)
        self.fc2 = nn.Linear(width_size, num_classes, bias=False)

    def forward(self, x):
        out = self.fc1(x)
        out = self.fc2(out)
        return out


# Initialize network
net = Net(input_size, width_size, num_classes)
net.fc1.weight.data = torch.from_numpy(M1)
net.fc2.weight.data = torch.from_numpy(M2)
optimizer = torch.optim.SGD(net.parameters(), lr=learning_rate)
# Define custom input
xx_a = input_vector.transpose()
xx_t = torch.from_numpy(xx_a)
xx_t.requires_grad = True
xx = Variable(xx_t)
# Define custom label
yy_a = label_vector.transpose()
yy_t = torch.from_numpy(yy_a)
yy_t.requires_grad = True
yy = Variable(yy_t)
# Forward pass to get output
output = net(xx)
# Obtain loss
optimizer.zero_grad()
criterion = nn.MSELoss()
l1_loss = 0
l2_loss = 0
l1_crit = torch.nn.L1Loss(size_average=False)
l2_crit = torch.nn.MSELoss(size_average=False)
for param in net.parameters():
    l1_loss += l1_crit(param, torch.zeros_like(param))
    l2_loss += 0.5*l2_crit(param, torch.zeros_like(param))
loss = criterion(output, yy) + l1_coef * l1_loss + l2_coef * l2_loss
# Get new gradients
loss.backward()
# Update parameters
optimizer.step()


print "Test parameters: Learning Rate = " + str(learning_rate) + "; L1 penalty coefficient: " + str(l1_coef) + "; L2 penalty coefficient: " + str(l2_coef)
print "///////////////////////////////////////////////////////////////////////////////////////////////////////"
print "Analytical output: " + str(torch.from_numpy(O_g))
print "Analytical loss: " + str(torch.from_numpy(loss_g))
print "Analytical M1 weights: " + str(torch.from_numpy(new_M1))
print "Analytical M2 weights: " + str(torch.from_numpy(new_M2))
print "///////////////////////////////////////////////////////////////////////////////////////////////////////"
print "Pytorch output: " + str(output.data.transpose(0, 1))
print "Pytorch loss: " + str(loss.data)
print "Pytorch M1 weights: " + str(net.fc1.weight.data)
print "Pytorch M2 weights: " + str(net.fc2.weight.data)
print "///////////////////////////////////////////////////////////////////////////////////////////////////////"