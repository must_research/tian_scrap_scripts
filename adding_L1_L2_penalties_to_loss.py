def get_total_loss(loss_f, y_hat, y, model, reg_co):
    total_loss = loss_f(y_hat, y)
    l1_loss = 0
    l2_loss = 0
    if reg_co[0] > 0:
        l1_coef = reg_co[0]
        l1_crit = torch.nn.L1Loss(size_average=False)
        for param in model.parameters():
            if len(param.data.shape) > 1:
                l1_loss += l1_crit(param, torch.zeros_like(param))
        l1_loss *= l1_coef
    if reg_co[1] > 0:
        l2_coef = reg_co[1]
        l2_crit = torch.nn.MSELoss(size_average=False)
        for param in model.parameters():
            if len(param.data.shape) > 1:
                l2_loss += 0.5 * l2_crit(param, torch.zeros_like(param))
        l2_loss *= l2_coef

    total_loss += l1_loss
    total_loss += l2_loss
    return total_loss